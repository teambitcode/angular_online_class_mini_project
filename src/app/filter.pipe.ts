import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(studentArray: any[], searchText: string): any[] {
    console.log("asdsadas asdsadsa &&&&&&");
    let tempArray: any[] = [];
    if (searchText) {
      for (let index = 0; index < studentArray.length; index++) {
        const element = studentArray[index];

        if (element.name.indexOf(searchText) > -1) {
          tempArray.push(element);
        }
      }
    } else {
      return studentArray;
    }

    return tempArray;
  }

}
