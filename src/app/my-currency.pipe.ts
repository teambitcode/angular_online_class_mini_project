import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myCurrency'
})
export class MyCurrencyPipe implements PipeTransform {

  transform(value: any): unknown {
    console.log('angular pipe***');
    return value + "$";
  }

}
