import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() muValue: any = "";

  @Output() tableOutputEvent = new EventEmitter<any>();

  searchText:string = "";

  num: number = 10;
  typeValue: any = "";
  fontCheck: boolean = true;
  studentArray: any = [
    {
      "name": "sadasdsadas",
      "age": 34,
      "Subject": "english"
    },
    {
      "name": "gfhjh",
      "age": 50,
      "Subject": "GGGG"
    },
    {
      "name": "bnbnv",
      "age": 19,
      "Subject": "FFFFF"
    },
    {
      "name": "ktkhgk",
      "age": 10,
      "Subject": "VVVV"
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  changeToLarge(): void {
    console.log("changeToLarge ****");
    this.fontCheck = false;

  }
  changeToSmall(): void {
    console.log("changeToSmall ****");
    this.fontCheck = true;
  }

  valueChange(eventValue: any) {
    // this.tableOutputEvent.emit(eventValue.target.value);
    this.searchText = eventValue.target.value;
  }


  filterTable() {
    console.log("asdsadas asdsadsa &&&&&&");
    let tempArray: any[] = [];

    for (let index = 0; index < this.studentArray.length; index++) {
      const element = this.studentArray[index];

      if (element.name.indexOf(this.searchText) > -1) {
        tempArray.push(element);
      }
    }

    return tempArray;
  }
}
