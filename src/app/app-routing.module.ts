import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { ProdutsTableComponent } from './produts-table/produts-table.component';
import { TableComponent } from './table/table.component';

// landing child routes
const landingChildRoutes : Routes = [
  {
    path: 'table',
    component: ProdutsTableComponent
  },
  {
    path: 'cart',
    component: CartComponent
  }
];


const routes: Routes = [
  {
    path: "",
    component: LoginComponent
  },
  {
    path: "main-table",
    component: TableComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "landing",
    component: LandingComponent,
    children:landingChildRoutes
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
