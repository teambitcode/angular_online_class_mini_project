import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-produts-table',
  templateUrl: './produts-table.component.html',
  styleUrls: ['./produts-table.component.scss']
})
export class ProdutsTableComponent implements OnInit, AfterViewInit, OnDestroy {
  productsArray: any[] = [];
  productName: string = "";
  productDescription: string = "";
  productPrice: number = 0;

  isCreateProductFrom:boolean = true;

  currentId:string = "";
  constructor(public _dataService: DataService) {
    console.log("constructor stated.....");
  }

  ngOnInit(): void {
    console.log("oninit stated.....");
    this.getAllProducts();
  }
  ngAfterViewInit(){
    console.log("ngAfterViewInit stated.....");
  }
  ngOnDestroy(): void {
    console.log("ngOnDestroy stated.....");
  }
  createProduct(): void {
    let product = {
      "name": this.productName,
      "description": this.productDescription,
      "price": this.productPrice
    }
    this._dataService.postApi(product, "product/new").subscribe((responseData: any) => {
      // console.log(responseData);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'New product has been saved',
        showConfirmButton: false,
        timer: 1500
      });
      this.getAllProducts();
    });

  }


  getAllProducts() {
    this._dataService.getApi("product/getAll").subscribe((responseValue: any) => {
      // console.log(responseValue);
      // console.log("responseValue ****");
      this.productsArray = responseValue.data;
    });
  }

  deleteProduct(id: string) {
    // console.log(id);
    this._dataService.deleteApi("product/remove/",id).subscribe((responseDelete:any)=>{
      Swal.fire({
        position: 'top-start',
        icon: 'success',
        title: 'Product has been deleted',
        showConfirmButton: false,
        timer: 1500
      });
      this.getAllProducts();
    });
  }

  setProductToFrom(productItem:any){
    this.productName = productItem.name;
    this.productDescription = productItem.description;
    this.productPrice = productItem.price;
    this.currentId = productItem._id;
    this.isCreateProductFrom = false;
  }

  reset(){
    this.productName ="";
    this.productDescription = "";
    this.productPrice = 0;

    this.isCreateProductFrom = true;

  }

  updateProduct(){
    let product = {
      "name": this.productName,
      "description": this.productDescription,
      "price": this.productPrice
    }
    this._dataService.updateProduct(product,"product/update/",this.currentId).subscribe((responseUpdate:any)=>{
      Swal.fire({
        position: 'top-start',
        icon: 'success',
        title: 'Product has been updated',
        showConfirmButton: false,
        timer: 1500
      });
      this.getAllProducts();
    });
  }
}
