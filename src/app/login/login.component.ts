import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // private router: Router = new Router();
  private age: number = 0;
  isLogin: boolean = true;

  userEmail:any;
  userPassword:any;
  userFirstName:any;
  userLastName:any;
  // private router: Router = new Router();

  constructor(private router: Router, public _dataService: DataService) { }

  ngOnInit(): void {
  }

  navigateToLanding(): void {
    this._dataService.userName = "teambitcode";
    this.router.navigateByUrl("landing");
  }

  login(): void {
    let user = {
      "email": this.userEmail,
      "password": this.userPassword
    }

    this._dataService.postApi(user,"user/login").subscribe((response: any) => {
      console.log(response.data.status);
      if (response.data.status) {
        this.navigateToLanding()
      }
    });

    // this.http.post("http://64.227.28.237:5000/user/login", user).subscribe((response: any) => {
    //   console.log(response.data.status);
    //   if (response.data.status) {
    //     this.navigateToLanding()
    //   }
    // });

  }

  changeTab(isLoginType: boolean) {
    this.isLogin = isLoginType;
  }


  createUser(): void {
    let user = {
      "email": this.userEmail,
      "role": "user",
      "password": this.userPassword,
      "first_name": this.userFirstName,
      "last_name": this.userLastName
    }
    console.log("dataResponse");
    this._dataService.postApi(user,"user/new").subscribe(dataResponse => {
      console.log(dataResponse);
    });

    // this.http.post("http://64.227.28.237:5000/user/new", user).subscribe(dataResponse => {
    //   console.log(dataResponse);
    // });
    console.log("dataResponse");
    console.log("dataResponse");
    console.log("dataResponse");
    console.log("dataResponse");

  }
}
