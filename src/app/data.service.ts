import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
public userName:string = "";
  constructor(private http: HttpClient) { }


  calculate(firstNum:number, secondNum:number): number{
    let result = firstNum + secondNum;
    return result;
  }


  postApi(body:any, path:string){ 
   return this.http.post("http://64.227.28.237:5000/"+path, body);
  }

  getApi(path:string){
    return this.http.get("http://64.227.28.237:5000/"+path);
  }

  deleteApi(path:string,id:string){
    return this.http.delete("http://64.227.28.237:5000/"+path+id);
  }

  updateProduct(body:any, path:string,id:string){
    return this.http.put("http://64.227.28.237:5000/"+path+id,body);

  }

}
