import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {
  @Input() message:string = '****';
  
  constructor() { 
    console.log("constructor started...");
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    console.log("ngOnChanges started...");
  }

  ngOnInit(): void {
    console.log("ngOnInit started...");
  }


}
