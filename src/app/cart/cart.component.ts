import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  newMessage:string = "###";
  constructor() { }

  ngOnInit(): void {
  }

  setName(prm1:number): string {
    console.log("call set name ");
    return prm1 + "$";
  }
  valueChange(dataEvent:any){
    // console.log(dataEvent.target.value);
    this.newMessage = dataEvent.target.value;
  }
}
